using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroysProjectiles : MonoBehaviour {

    Sprite explosion;

    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "Projectile") {
            StartCoroutine("DestroyAnimation", coll.gameObject);
        }
    }

    IEnumerator DestroyAnimation(GameObject toDestroy) {
        yield return new WaitForSeconds(1.5f);
        Destroy(toDestroy);
        yield return null;
    }

}
