﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanFire : MonoBehaviour {

    public GameObject bullet;
    public bool allowedToFire = false;
    public float power = 5f;
    private const float POWER_MODIFER = 100f;


    void Update () {
        if (!allowedToFire)
            return;

        if (Input.GetButtonUp("Fire1")) {
            var bulletClone = Instantiate(bullet, transform.position, transform.rotation);
            bulletClone.GetComponent<CircleCollider2D>().isTrigger = true;
            var clickPos = GetWorldPosition(Input.mousePosition);
            var direction = GetDirectionFromTo(transform.position, clickPos);
            direction.z = 0;
            bulletClone.GetComponent<Rigidbody2D>().AddForce(direction.normalized * power * POWER_MODIFER);
            StartCoroutine("TryArmBullet", bulletClone);
        }
    }

    // Wait until bullet is out of base
    IEnumerator TryArmBullet(GameObject bullet) {
        var baseBounds = gameObject.GetComponent<BoxCollider2D>().bounds;
        while (baseBounds.Intersects(bullet.GetComponent<CircleCollider2D>().bounds))
        {
            yield return new WaitForSeconds(0.1f);
        }
        TurnManager.Instance.NextPlayer();
        bullet.GetComponent<CircleCollider2D>().isTrigger = false;
        yield return null;
    }


    Vector3 GetDirectionFromTo(Vector3 src, Vector3 des) {
        return des - src;
    }

    Vector3 GetWorldPosition(Vector3 inputClick, float zIndex = 0) {
        inputClick.z = zIndex;
        return Camera.main.ScreenToWorldPoint(inputClick);
    }

    //Graveyard
    int GetLayerMaskAtIndex(int index) {
        // Bitshift to get the correct layermask at index  index6)
        return 1 << index;
    }

    Vector3 GetRaycastPosition(Vector3 inputClick) {
        Ray ray = Camera.main.ScreenPointToRay(inputClick);
        RaycastHit hit;
        // A 'RaycastLayer' Layer must be create else this will not return true;
        if (Physics.Raycast(ray, out hit, 100f, GetLayerMaskAtIndex(8)))
        {
            return hit.point;
        }
        throw new ApplicationException("Did not hit a raycast layer");
    }

}
