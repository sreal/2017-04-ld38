using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour {

    public float maxHealth = 1f;
    public Sprite originalSprite;
    public Sprite damagedSprite;

    private float currentHealth;

    void Awake() {
        originalSprite = gameObject.GetComponent<SpriteRenderer>().sprite;
        Reset();
    }

    void OnCollisionEnter2D(Collision2D coll) {
        var damage = coll.gameObject.GetComponent<DealsDamage>();
        if (damage != null && currentHealth > 0) {
            ApplyDamage(damage.amount);
            Destroy(coll.gameObject);
        }
    }

    void ApplyDamage(float amount) {
        currentHealth -= amount;
        if (currentHealth <= 0) {
            gameObject.GetComponent<SpriteRenderer>().sprite = damagedSprite;

            // Assume 2 player
            TurnManager.Instance.PlayerDeath();
        }
    }

    public void Reset() {
        currentHealth = maxHealth;
        gameObject.GetComponent<SpriteRenderer>().sprite = originalSprite;
    }

}
