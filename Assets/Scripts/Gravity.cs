﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gravity : MonoBehaviour
{
    public float gravitationalPull = 9.8f;
    private const float POWER_MODIFIER = 5f;

    void FixedUpdate ()
    {
        List<GameObject> others = new List<GameObject>();
        foreach (GameObject other in UnityEngine.Object.FindObjectsOfType<GameObject>()) {
            if (other == gameObject)
                continue;
            var rigidbody = other.GetComponent<Rigidbody2D>();
            if (rigidbody == null)
                continue;
            others.Add(other);
        }
        foreach (GameObject other in others) {
            var direction = (gameObject.transform.position - other.transform.position);
            other.GetComponent<Rigidbody2D>().AddForce(direction.normalized * (1 / direction.magnitude) * gravitationalPull * POWER_MODIFIER);
        }

    }

}
