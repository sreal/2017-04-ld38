﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotates : MonoBehaviour {

    public float speed = 10f;

    void Update() {
        transform.Rotate(Vector3.forward * Time.deltaTime * speed);
        // var newRot = transform.rotation;
        // var td = Time.deltaTime;
        // var newZ = newRot.z + (speed * td);

        // Debug.LogFormat("z:{0}, td:{1}, nz:{2}", newRot.z, td, newZ);

        // newRot.z = newZ;
        // transform.rotation = newRot;
    }

}
