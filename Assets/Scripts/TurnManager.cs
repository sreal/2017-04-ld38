﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnManager : MonoBehaviour {

    public GameObject[] players;
    public int turnIndex = 0;

    public Button resetButton;
    float buttonHideOffset = 500f;

    bool inGame = true;

    public static TurnManager Instance { get; private set; }
    private void Awake() {
        if (Instance != null) {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        HideButton(resetButton);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }

        for(var i = 0; i < players.Length; i++) {
            var player = players[i];
            var canFire = player.GetComponent<CanFire>();
            canFire.allowedToFire = inGame && (i == turnIndex);
        }
    }

    public void NextPlayer() {
        turnIndex++;
        if (turnIndex >= players.Length)
            turnIndex = 0;

        Debug.Log(turnIndex);
    }

    void DestroyBullets() {
        foreach (GameObject other in UnityEngine.Object.FindObjectsOfType<GameObject>()) {
            if (other.gameObject.tag == "Projectile") {
                Destroy(other);
            }
        }
    }
    public void Reset() {
        DestroyBullets();
        for(var i = 0; i < players.Length; i++) {
            var player = players[i];
            var takeDamage = player.GetComponent<TakeDamage>();
            takeDamage.Reset();
        }
        turnIndex = 0;
        StartCoroutine("EnablePlay");

    }

    public void PlayerDeath() {
        inGame = false;
        ShowButton(resetButton);
        for (var i = 0; i < players.Length; i++) {
            players[i].GetComponent<CanFire>().allowedToFire = false;
        }
    }

    IEnumerator EnablePlay() {
        yield return new WaitForSeconds(.1f);
        HideButton(resetButton);
        inGame = true;
        yield return null;
    }

    void ShowButton(Button but){
        but.gameObject.SetActive(true);
    }
    void HideButton(Button but){
        but.gameObject.SetActive(false);
    }

}
